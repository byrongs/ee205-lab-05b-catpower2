###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Byron Soriano <byrongs@hawaii.edu>
### @date 15_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC = g++
CFLAGS = -g -Wall -Wextra

TARGET = catPower

all: $(TARGET)

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

catPower.o: catPower.cpp ev.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o ev.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o

j.l: j.cpp j.h
	$(CC) $(CFLAGS) -c j.cpp

catPower.o: catPower.cpp j.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o j.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o j.o

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp

catPower.o: catPower.cpp megaton.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o megaton.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o megaton.o

gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp

catPower.o: catPower.cpp gge.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o gge.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o gge.o

foe.o: foee.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

catPower.o: catPower.cpp foe.h
	$(CC) $(CFLAGS) -c catPower.cpp
	
catPower: catPower.o foe.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o foe.o

cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp

catPower.o: catPower.cpp cat.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o cat.o
clean:
	rm -f $(TARGET) *.o


